import React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import 'react-native-gesture-handler';

import Login from './Navigation/Login';
import Home from './Navigation/Home';
import Splash from './Navigation/Splash';
import Register from './Navigation/Register';
import Profile from './Navigation/Profile'

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const HomeStack = createBottomTabNavigator();
const ProfileStack = createBottomTabNavigator();

const ProfileStackScreen = () => (
    <ProfileStack.Navigator>
      <ProfileStack.Screen name='Profile' component={Profile} />
    </ProfileStack.Navigator>
)

const HomeStackScreen = () => (
    <HomeStack.Navigator>
      <HomeStack.Screen name='Home' component={Home} />
    </HomeStack.Navigator>
)
const jjgjgjk = () => {
  return (
    <NavigationContainer>
      <Tabs.Navigator>
        <Tabs.Screen name='Home' component={HomeStackScreen} />
        <Tabs.Screen name='Profile' component={ProfileStackScreen} />
      </Tabs.Navigator>
    </NavigationContainer>
  )
}
export default () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash" >
        <Stack.Screen name='Splash' component={Splash} options={{ headerShown: false }}/>
        <Stack.Screen name='Login' component={Login} options={{ headerShown: false }} />
        <Stack.Screen name='Home' component={Home} options={{ headerTitle: 'Daftar Barang' }} />
        <Stack.Screen name='Register' component={Register} options={{ headerShown: true }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}