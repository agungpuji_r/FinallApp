import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    Text,
    Button,
    TextInput,
} from 'react-native';

import { AntDesign } from '@expo/vector-icons';

export default class App extends Component {
    render() {
        return (
        <View style={styles.container}>
            <View style={styles.fotoProfil}>
                <Text style={{
                      fontSize: 24,
                      fontWeight: 'bold',
                      color: '#003366'
                }}>Tentang Saya</Text>
                {/* <Image source={require('./images/image.jpg')} style={{width: 100, height: 100, borderRadius: 100/ 2}} 
/> */}
            </View>
            <View style={styles.pengembang}>
                <Text style={{
                    color: '#003366',
                    fontWeight: 'bold'
                }}>Agung Puji Raharjo</Text>
                <Text style={{
                    color: '#3EC6FF',
                    fontweight: 'bold',
                    fontSize: 12
                }}>React Native Developer</Text>
            </View>
            <View style={styles.box1}>
                <Text style={{
                    color: '#003366',
                    margin: 5,
                    borderBottomColor: '#003366',
                    borderBottomWidth: 1
                }}>Portofolio</Text>
                <View style={{
                    alignItems: 'center',
                    marginBottom: 10
                }}>                
                    <AntDesign name="gitlab" size={45} color="#3EC6FF"/>
                    <Text style={{color: '#003366', fontWeight: 'bold'}}>@agungpuji_r</Text>
                    </View>
            </View>
            <View style={styles.box2}>
                <Text style={{
                    color: '#003366',
                    margin: 5,
                    borderBottomColor: '#003366',
                    borderBottomWidth: 1
                }}>Hubungi Saya</Text>
                <View style={styles.sosmed}>
                    <View style={styles.align}><AntDesign name="instagram" size={30} color="#3EC6FF" />
                    <Text style={{color: '#003366', fontSize: 15}}> @agungpuji_r</Text>
                    </View>
                </View>
            </View>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    fotoProfil: {
        alignItems: 'center',
        margin: 30
    },
    pengembang: {
        alignItems: 'center',
        marginBottom: 5
    },
    box1: {
        backgroundColor: '#EFEFEF',
        margin: 10,
        borderRadius: 10,     
    },
    box2: {
        backgroundColor: '#EFEFEF',
        margin: 10,
        borderRadius: 10,
    },
    sosmed: {
        alignItems: 'center'
    },
    align: {
      flexDirection: 'row',
      marginBottom: 10,
      marginTop: 10,
      justifyContent: 'flex-start'
    }
});