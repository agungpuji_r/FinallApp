import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

const Splash = ({navigation}) => {
    useEffect(()=>{
        setTimeout(()=>{
            navigation.replace("Login")
        }, 3000)
    }, [])

    return (
        <View style={styles.container}>
            <View style={styles.box}>
        <View style={{justifyContent:'center', alignItems:'center'}}>
        <Image source={require('./Components/Logo.png')} style={{ width: 300, height: 100, resizeMode: 'contain' }} />
        <Text style={{ fontSize: 24, fontWeight: 'bold', color: 'white'}}>Parabot</Text>
        </View></View>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      resizeMode: 'cover',
      backgroundColor: '#91e0ac'
    },
    box: {
      width: 200,
      height: 200,

    },
  })
