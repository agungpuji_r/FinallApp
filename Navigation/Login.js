import React from 'react';
import { View, Text, TextInput, StyleSheet, Button, Image } from 'react-native';
import { color } from 'react-native-reanimated';
import Home from './Home';
import Register from './Register';

export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
    }
  }

  loginHandler() {
    console.log(this.state.userName, ' ', this.state.password)
    if (this.state.password == '') {
      console.log('login berhasil')
      this.setState({ isError : false });
      this.props.navigation.navigate( 'Home', {
        userName: this.state.userName 
      })
    } else {
      console.log('password salah')
      this.setState({ isError : true })
    } 
  }

  render() {
    return (
      <View style={styles.container}>
        <View>
        <Image source={require('./Components/Logo.png')} style={{ width: 300, height: 100, resizeMode: 'contain' }} />
        <Text style={styles.subTitleText}>Parabot</Text>
        </View>

        <View style={styles.formContainer}>
          <View style={styles.inputContainer}>
            <View>
              <Text style={styles.labelText}>Username/Email</Text>
              <TextInput
                style={styles.textInput}
                placeholder='Masukkan Nama User/Email'
                onChangeText={userName => this.setState({ userName })}
              />
            </View>
          </View>

          <View style={styles.inputContainer}>
            <View>
              <Text style={styles.labelText}>Password</Text>
              <TextInput
                style={styles.textInput}
                placeholder='Masukkan Password'
                onChangeText={password => this.setState({ password })}
                secureTextEntry={true}
              />
              <Text style={{fontSize: 12}} onPress={() => this.props.navigation.navigate('Register')}>Belum Punya Akun? Daftar</Text>
            </View>
          </View>
          <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Password Salah</Text>
          <Button title='Login' onPress={() => this.loginHandler()} />
        </View>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#91e0ac'
  },
  titleText: {
    fontSize: 48,
    fontWeight: 'bold',
    color: 'blue',
    textAlign: 'center',
  },
  subTitleText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
    alignSelf: 'center',
    marginBottom: 16,
  },
  formContainer: {
    justifyContent: 'center'
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 16
  },
  labelText: {
    fontWeight: 'bold'
  },
  textInput: {
    width: 300,
    backgroundColor: 'white'
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginBottom: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginBottom: 16,
  }
});
